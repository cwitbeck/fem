from FEM import FEM
from FEM import Mesh
import numpy as np

points = [(0,0),(2,0),(2,1),(1,1),(1,2),(0,2)]
mesh = Mesh(points,max_area=0.001)

f = lambda x: np.sin(2*np.pi*np.arctan(x[1]/x[0]))
p = lambda x:x[1]
q = lambda x:x[0]
r = lambda x:-x[0]**2-x[1]**2
g = lambda x:np.exp(-x[0]**2-x[1]**2)#lambda x:x[0]*x[1]
V = FEM(mesh,f=f,p=p,q=q,r=r,g=g)
V.show()
V.contour()
