from FEM import FEM
from FEM import Mesh
import numpy as np
from numbers import Number as number

points = [(2,2),(-2,2),(-2,-2),(2,-2)]
mesh = Mesh(points,max_area=0.01)

def f(x):
    f = lambda x: 10 if x[0]**2+x[1]**2<=0.25 else 0
    if isinstance(x[0],number):
        return f(x)
    return np.array([f([x[0][i],x[1][i]]) for i in range(len(x[0]))])

V = FEM(mesh,f=f)
V.show()
V.contour()
