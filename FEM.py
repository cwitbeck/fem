import numbers
from tqdm import trange
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as pt
import matplotlib.cm as cm
import meshpy.triangle as mp_tri
import numpy as np
import quadpy as qp
from functools import partial
from functools import reduce
from itertools import combinations as comb
from scipy.sparse import csr_matrix
from scipy.sparse.linalg import spsolve

def share_edge(tri1,tri2):
    return len(set(tri1.points) & set(tri2.points))==2 

class triangle(object):
    def __init__(self,points,num=0):
        x1,x2,x3 = points[0][0],points[1][0],points[2][0]
        y1,y2,y3 = points[0][1],points[1][1],points[2][1]
        self.points = points
        self.centroid = [(x1+x2+x3)/3,(y1+y2+y3)/3]
        self.area = abs((x2-x1)*(y3-y1)-(y2-y1)*(x3-x1))/2
        self.index = num

    def inside(self,x,eps=10**(-12)):
        A = triangle([x,self.points[0],self.points[1]]).area
        B = triangle([x,self.points[0],self.points[2]]).area
        C = triangle([x,self.points[1],self.points[2]]).area
        return abs(self.area-(A+B+C))<eps

    def show(self):
        pt.triplot([x[0] for x in self.points],[y[1] for y in self.points])
        pt.show()
    
    def add_to_plot(self):
        pt.triplot([x[0] for x in self.points],[y[1] for y in self.points])

class lifted_triangle(triangle):
    def __init__(self,tri,lft_ind):
        super(lifted_triangle,self).__init__(tri.points,tri.index)
        op = [tri.points[lft_ind]] + [tri.points[i] for i in range(len(tri.points)) if i != lft_ind]
        dx1,dx2,dx3 = op[0][0]-op[1][0], op[0][0]-op[2][0], op[1][0]-op[2][0] 
        dy1,dy2,dy3 = op[0][1]-op[1][1], op[0][1]-op[2][1], op[1][1]-op[2][1]
        k = dx1*dy2 - dx2*dy1
        self.dx = dy3/k
        self.dy = -dx3/k
        self.lp = op[0]

    def z(self,x):
        if self.inside(x):
            return self.dx*(x[0]-self.lp[0])+self.dy*(x[1]-self.lp[1])+1
        else:
            return 0

    def show(self):
        x_min = min([x[0] for x in self.points])
        x_max = max([x[0] for x in self.points])
        y_min = min([y[1] for y in self.points])
        y_max = max([y[1] for y in self.points])
        X = np.arange(x_min,x_max,(x_max-x_min)/100)
        Y = np.arange(y_min,y_max,(y_max-y_min)/100)
        Z = np.empty(shape=(len(Y),len(X)))
        X,Y = np.meshgrid(X,Y)
        for i in range(len(Z)):
            for j in range(len(Z[0])):
                Z[i][j] = self.z(X[i][j],Y[i][j])
        fig = pt.figure()
        ax = fig.gca(projection="3d")
        surf = ax.plot_surface(X,Y,Z)
        pt.show()


class Mesh:
    def round_trip_connect(self,start, end):
        return [(i, i+1) for i in range(start, end)] + [(end, start)]

    def __init__(self,points,facets="",holes="",rf_func="",max_area=0.1):
        print("Generating mesh...")
        if facets == "":
            facets = self.round_trip_connect(0, len(points)-1)
        if rf_func == "":
            rf_func = lambda vertices, area: area > max_area
        info = mp_tri.MeshInfo()
        info.set_points(points)
        info.set_facets(facets)
        if holes != "":
            info.set_holes(holes)
        
        self.mesh = mp_tri.build(info, refinement_func=rf_func)
        self.nodes = np.array(self.mesh.points)
        self.elements = np.array(self.mesh.elements)
        
        edges = []
        for x in self.elements:
            for p in comb({0,1,2},2):
                e = frozenset({x[p[0]],x[p[1]]})
                if e in edges:
                    edges.remove(e)
                else:
                    edges.append(e)

        self.bd_index = reduce(lambda u,v:u.union(v),edges)
        self.int_index = {i for i in range(0,len(self.nodes))}-self.bd_index
        self.bd_nodes = np.array([self.nodes[i] for i in self.bd_index])
        self.int_nodes = np.array([self.nodes[i] for i in self.int_index])

    def show(self):
        pt.triplot(self.nodes[:, 0],self.nodes[:, 1], self.elements)
        pt.scatter(self.int_nodes[:,0],self.int_nodes[:,1],c="red",zorder=2)
        pt.scatter(self.bd_nodes[:,0],self.bd_nodes[:,1],c="blue",zorder=2)
        pt.show()

class piecewise_linear:
    def __init__(self,lftd_tris,bd=True,scheme=qp.t2.get_good_scheme(6)):
        self.bd = bd
        self.scheme = scheme
        self.triangle_dict = {T.index:T for T in lftd_tris}
        points = []
        for T in self.triangle_dict:
            points += self.triangle_dict[T].points
        X = [x[0] for x in points]
        Y = [y[0] for y in points]
        self.xmin, self.xmax, self.ymin, self.ymax = min(X),max(X),min(Y),max(Y)
        self.peak = list(self.triangle_dict.values())[0].lp

    def eval(self,x):
        def e(x):
            for t in self.triangle_dict:
                if self.triangle_dict[t].inside(x):
                    return self.triangle_dict[t].z(x)
            return 0
        if isinstance(x[0],numbers.Number):
            return e(x)
        return np.array([e([x[0][i],x[1][i]]) for i in range(len(x[0]))])

    def integrate(self,f):
        tris = np.array([self.triangle_dict[i].points for i in self.triangle_dict])
        return sum(map(lambda x: self.scheme.integrate(f,x),tris))

class FEM:
    def p_int(self,T1,T2):
        T1p = np.array(T1.points)
        p = self.p
        if p=="":
            return T1.dx*T2.dx*T1.area
        elif isinstance(p,numbers.Number):
            return p*T1.dx*T2.dx*T1.area
        else:
            return T1.dx*T2.dx*self.scheme.integrate(p,T1p)
    
    def q_int(self,T1,T2):
        T1p = np.array(T1.points)
        q = self.q
        if q=="":
            return T1.dy*T2.dy*T1.area
        elif isinstance(q,numbers.Number):
            return q*T1.dy*T2.dy*T1.area
        else:
            return T1.dy*T2.dy*self.scheme.integrate(q,T1p)
    
    def r_int(self,P1,P2,T1p):
        scheme = qp.t2.get_good_scheme(6)
        r = self.r
        if r=="":
            return 0
        elif isinstance(r,numbers.Number):
            return r * self.scheme.integrate(lambda x: P1.eval(x)*P2.eval(x),T1p)
        else:
            return self.scheme.integrate(lambda x: r(x)*P1.eval(x)*P2.eval(x),T1p)

    def basis_integrator(self,P1,P2):
        S = P1.triangle_dict.keys() & P2.triangle_dict.keys()
        if S == set():
            return 0
        T1,T2 = P1.triangle_dict,P2.triangle_dict
        integral = lambda i: self.p_int(T1[i],T2[i])+self.q_int(T1[i],T2[i])-self.r_int(P1,P2,np.array(T1[i].points))
        return sum(map(integral, S))

    def create_K(self):
        print("Creating K matrix... ")
        K = []
        for i in trange(len(self.basis)):
            a = self.basis[i]
            if not a.bd:
                K.append([])
                for b in self.basis:
                    if not b.bd:
                        K[-1].append(self.basis_integrator(a,b))
        return csr_matrix(K)

    def boundary(self,P):
        g = self.g
        integral = 0
        for Q in self.basis:
            if Q.bd:
                if g=="":
                    return 0
                elif isinstance(g,numbers.Number):
                    integral += g*self.basis_integrator(P,Q)
                else:
                    integral += g(Q.peak)*self.basis_integrator(P,Q)
        return integral

    def create_F(self):
        print("Creating load vector... ")
        F = []
        for i in trange(len(self.basis)):
            P = self.basis[i]
            if not P.bd:
                F.append(P.integrate(self.f)-self.boundary(P))
        return np.array(F)
    
    def __init__(self,mesh,f,p="",q="",r="",g="", scheme = qp.t2.get_good_scheme(6)):
        self.mesh = mesh
        self.scheme = scheme
        self.f,self.p,self.q,self.r,self.g = f,p,q,r,g
        self.support = [[] for n in mesh.nodes]
        for i in range(len(mesh.elements)):
            T = mesh.elements[i]
            tri = triangle([mesh.nodes[i] for i in T],i)
            self.support[T[0]].append(lifted_triangle(tri,0))
            self.support[T[1]].append(lifted_triangle(tri,1))
            self.support[T[2]].append(lifted_triangle(tri,2))
        self.basis = [piecewise_linear(self.support[i],i in self.mesh.bd_index,scheme) for i in range(len(self.support))]
        self.K = self.create_K()
        self.F = self.create_F()
        #self.a = np.linalg.solve(self.K,self.F)
        print("Solving linear system...")
        self.a_int = spsolve(self.K,self.F)
        if g=="":
            self.a_bd = [0 for i in mesh.bd_nodes]
        elif isinstance(g,numbers.Number):
            self.a_bd = [g for i in mesh.bd_nodes]
        else:
            self.a_bd = [g(x) for x in mesh.bd_nodes]
        for i in range(len(self.a_bd)):
            x = mesh.bd_nodes[i]

    def eval(self,x,debug=False):
        f = 0
        i,j = 0,0
        for p in self.basis:
            if not p.bd:
                f += self.a_int[i]*p.eval(x)
                i += 1
            if p.bd:
                if self.g != "":
                    f += self.g(p.peak)*p.eval(x)
                    j += 1
        return f

    def show(self):
        fig = pt.figure()
        ax = fig.gca(projection="3d")
        X,Y,Z = [],[],[]
        for d in self.mesh.nodes:
            X.append(d[0])
            Y.append(d[1])
            Z.append(self.eval(d))
        ax.plot_trisurf(X,Y,self.mesh.elements,Z,linewidth=0.2,antialiased=True)
        pt.show()

    def contour(self,cmap=cm.get_cmap(name='jet', lut=None)):
        X,Y,Z = [],[],[]
        for d in self.mesh.nodes:
            X.append(d[0])
            Y.append(d[1])
            Z.append(self.eval(d))
        pt.tricontourf(X,Y,self.mesh.elements,Z,levels=255,cmap=cmap)
        pt.colorbar()
        pt.show()
