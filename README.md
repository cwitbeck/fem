# README #

### What is this repository for? ###

This repository was created for my final project in MA760 Numerical Analysis at Emporia State University. It is an 2D implementation of the finite element method. 
It uses meshpy to generate a mesh and then uses numpy, scipy and quadpy to create and solve the linear equations in FEM. 

### How do I get set up? ###

Dependencies:

+ meshpy
+ scipy
+ quadpy
+ matplotlib
+ tqdm