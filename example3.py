from FEM import FEM
from FEM import Mesh
import numpy as np

def round_trip_connect(start,end):
    return [(i,i+1) for i in range(start,end)] + [(end,start)]

points = [(1,1),(-1,1),(-1,-1),(1,-1)]
facets = round_trip_connect(0,len(points)-1)
circ_start = len(points)

points.extend((3*np.cos(theta), 3*np.sin(theta)) for theta in np.linspace(0,2*np.pi,30,endpoint=False))
facets.extend(round_trip_connect(circ_start, len(points)-1))
holes = [(0,0)]

mesh = Mesh(points=points,facets=facets,holes=holes,max_area=0.01)

f = lambda x: np.sin(2*np.pi*np.arctan(x[1]/x[0]))
p = lambda x:abs(x[1])
q = lambda x:abs(x[0])
r = lambda x:-x[0]**2-x[1]**2
g = lambda x:np.exp(-x[0]**2-x[1]**2)#lambda x:x[0]*x[1]
V = FEM(mesh,f=f,p=p,q=q,r=r,g=g)
V.show()
V.contour()
